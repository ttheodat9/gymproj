var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const router = express.Router();
//Cross-origin resource sharing (CORS) allows AJAX requests 
//to skip the Same-origin policy and access resources from remote hosts.
var cors = require('cors');

var indexRouter = require('./routes/index');
var clientsRouter = require('./routes/clients');
var instructorsRouter = require('./routes/instructors');
var groupsRouter = require('./routes/groups');
var inscriptionsRouter = require('./routes/inscriptions');


var app = express();
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/clients', clientsRouter);
app.use('/instructors', instructorsRouter);
app.use('/groups', groupsRouter);
app.use('/inscriptions', inscriptionsRouter);
 

module.exports = app;
