var express = require('express');
var router = express.Router();
const mongodb = require('mongodb');
const mongodbClient = require('mongodb').MongoClient;

const mongoUrl = 'mongodb://localhost:27017/teriGym';


router.get('/', (req, res) => {
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');

    dbObject.collection('clients').find().toArray((error, result) => {
      if (error) throw error;
      var clientResult = JSON.stringify(result);

      dbObject.collection('groups').find().toArray((error, result) => {
        if (error) throw error;
        var groupResult = JSON.stringify(result);
  

      dbObject.collection('instructors').find().toArray((error, result) => {
        if (error) throw error;
        db.close();
        res.send({
          clients: clientResult,
          groups: groupResult,
          instructors: JSON.stringify(result)
        });
      })
    })
  })
  })
});

router.post('/insertclient', (req, res) => {
  let clientForm = req.body;
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');

    dbObject.collection('clients').insertOne(clientForm, (error, result) => {
      if (error) throw error;
      db.close();
      res.end();
    })
  })
}); 

router.put('/updateclient/:id', (req, res) => {
  let clientForm = {$set: req.body};
  let theId = {_id: new mongodb.ObjectID(req.params.id)};
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    let dbObject = db.db('teriGym');

    if (error) throw error;
    dbObject.collection('clients').updateOne(theId, clientForm, (error, result) => {
      if(error) throw error;
      db.close();
      res.end();
    })
  })
});

router.delete('/removeclient/:id', (req, res) => {
  let theId = {_id: new mongodb.ObjectID(req.params.id)};
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');

    dbObject.collection('clients').deleteOne(theId, (error, result) => {
      if (error) throw error;
      db.close();
      res.end();
    })
  })
});

module.exports = router;
