var express = require('express');
var router = express.Router();
const mongodb = require('mongodb');
const mongodbClient = require('mongodb').MongoClient;

const mongoUrl = 'mongodb://localhost:27017/teriGym';


router.get('/', (req, res) => {
  mongodbClient.connect(mongoUrl, {useUnifiedTopology: true}, (error, db) => {
    if(error) throw error;
    let dbObject = db.db('teriGym');
    dbObject.collection('instructors').find().toArray((error, result) => {
      if(error) throw error;
      db.close();
      res.end(JSON.stringify(result));
    })
  })
});

router.post('/insertinstructor', (req, res) => {
  let instructorForm = req.body;
  mongodbClient.connect(mongoUrl, {useUnifiedTopology: true}, (error, db) => {
    if(error) throw error;
    let dbObject = db.db('teriGym');
    dbObject.collection('instructors').insertOne(instructorForm, (error, result) => {
      if(error) throw error;
      db.close();
      res.end();
    })
  })
});

router.put('/updateinstructor/:id', (req, res) => {
  let instructorForm =  {$set: req.body};
  let theId = {_id: new mongodb.ObjectID(req.params.id)};
  mongodbClient.connect(mongoUrl, {useUnifiedTopology: true}, (error, db) => {
    if(error) throw error;
    let dbObject = db.db('teriGym');
    dbObject.collection('instructors').updateOne(theId, instructorForm, (error, result) => {
      if(error) throw error;
      db.close();
      res.end();
    })
  })
});

router.delete('/removeinstructor/:id', (req, res) => {
  let theId = {_id: new mongodb.ObjectID(req.params.id)};
  mongodbClient.connect(mongoUrl, {useUnifiedTopology: true}, (error, db) => {
    if(error) throw error;
    let dbObject = db.db('teriGym');
    dbObject.collection('instructors').deleteOne(theId, (error, result) => {
      if(error) throw error;
      db.close();
      res.end();
    })
  });
});
module.exports = router;