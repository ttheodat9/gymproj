var express = require('express');
var router = express.Router();
const mongodb = require('mongodb');
const mongodbClient = require('mongodb').MongoClient;

const mongoUrl = 'mongodb://localhost:27017/teriGym';

router.get('/', (req, res) => {
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');

    dbObject.collection('groups').find().toArray((error, result) => {
      if (error) throw error;
      var groupResult = JSON.stringify(result);

      dbObject.collection('instructors').find().toArray((error, result) => {
        if (error) throw error;
        db.close();
        res.send({
          groups: groupResult,
          instructors: JSON.stringify(result)
        });
      })
    })
  })
});

router.post('/insertgroup', (req, res) => {
  let groupForm = req.body;
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');
    dbObject.collection('groups').insertOne(groupForm, (error, result) => {
      if (error) throw error;
      db.close();
      res.end();
    })
  })
});


router.put('/updategroup/:id', (req, res) => {
  let groupForm = {$set: req.body};
  let theId = { _id: new mongodb.ObjectID(req.params.id) };
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');
    dbObject.collection('groups').updateOne(theId, groupForm, (error, result) => {
      if (error) throw error;
      db.close();
      res.end();
    })
  })
});

router.delete('/removegroup/:id', (req, res) => {
  let theId = { _id: new mongodb.ObjectID(req.params.id) };
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym'); 
    dbObject.collection('groups').deleteOne(theId, (error, result) => {
      if (error) throw error;
      db.close();
      res.end();
    })
  })
});

module.exports = router;