var express = require('express');
var router = express.Router();
const mongodb = require('mongodb');
const mongodbClient = require('mongodb').MongoClient;

var options = {
  replSet: {
    sslValidate: false
  }
};

// const mongoUrl = 'mongodb://localhost:27017';
const url = 'mongodb+srv://terisha:theodat@cluster0.as3hc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
 