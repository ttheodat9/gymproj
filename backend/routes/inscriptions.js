var express = require('express');
var router = express.Router();
const mongodb = require('mongodb');
const mongodbClient = require('mongodb').MongoClient;

const mongoUrl = 'mongodb://localhost:27017/teriGym';

router.get('/', (req, res) => {
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');

    dbObject.collection('inscriptions').find().toArray((error, result) => {
      if (error) throw error;
      var inscriptionResult = JSON.stringify(result);
      
      dbObject.collection('clients').find().toArray((error, result) => {
        if (error) throw error;
        var clientResult = JSON.stringify(result);

        dbObject.collection('groups').find().toArray((error, result) => {
          if (error) throw error;
          var groupResult = JSON.stringify(result);


          dbObject.collection('instructors').find().toArray((error, result) => {
            if (error) throw error;
            db.close();
            res.send({
              inscriptions: inscriptionResult,
              clients: clientResult,
              groups: groupResult,
              instructors: JSON.stringify(result)
            });
          })
        })
      })
    })
  })
});

router.post('/insertinscription', (req, res) => {
  let inscriptionForm = req.body;
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');
    dbObject.collection('inscriptions').insertOne(inscriptionForm, (error, result) => {
      if (error) throw error;
      db.close();
      res.end();
    })
  })
});

router.put('/updateinscription/:id', (req, res) => {
  let inscriptionForm = { $set: req.body };
  let theId = { _id: new mongodb.ObjectID(req.params.id) };
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');
    dbObject.collection('inscriptions').updateOne(theId, inscriptionForm, (error, result) => {
      if (error) throw error;
      db.close();
      res.end();
    })
  })
});

router.delete('/removeinscription/:id', (req, res) => {
  let theId = { _id: new mongodb.ObjectID(req.params.id) };
  mongodbClient.connect(mongoUrl, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('teriGym');
    dbObject.collection('inscriptions').deleteOne(theId, (error, result) => {
      if (error) throw error;
      db.close();
      res.end();
    })
  });
});

module.exports = router;