import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../views/index.vue'
import clientPage from '../views/clients.vue'
import instuctorPage from '../views/instructors.vue'
import groupPage from '../views/groups.vue'
import inscriptionPage from '../views/inscriptions.vue'



//Used to define the base URL of all AJAX requests to the backend
//This is the URL where the Express server is listening for requests
//Made global to be used by the whole application
global.baseURL = 'http://localhost:1000'; 

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    component: index
  },
  {
    path: '/clients',
    name: 'clientPage',
    component: clientPage
  },
  {
    path: '/instructors',
    name: 'instuctorPage',
    component: instuctorPage
  },
  {
    path: '/groups',
    name: 'groupPage',
    component: groupPage
  },
  {
    path: '/inscriptions',
    name: 'inscriptionPage',
    component: inscriptionPage
  }
]

const router = new VueRouter({
  routes
})

export default router


